#!/usr/bin/env python3

import numpy as np
from scipy.spatial.transform import Rotation as R

# -------------------------------------------------------------------------------------------------------------------- #


def generate_point_cloud(nb_points: float) -> np.ndarray:
    """
    Given a number of point, generate a random nb_points point cloud.

    The returned np.array have the shape: N X 3 where:
    - N is the number of point in the point cloud
    """

    return np.random.random((nb_points, 3))


def transform_point(point: np.ndarray, transform: np.ndarray) -> np.ndarray:
    """
    Apply a tranformation to a given point.
    """

    return transform[:3, :3] @ point + transform[:3, 3]


def transform_point_cloud(point_cloud: np.ndarray, transform: np.ndarray) -> np.ndarray:
    """
    Given a point cloud (as a numpy np.array) and a homogeneous transformation matrix (as a numpy np.array), return the
    transformed point cloud (where the transformation matrix is applid to all point).

    The returned np.array have the shape: N X 3 where:
    - N is the number of point in the point cloud
    """

    transformed_point_could = np.zeros_like(point_cloud)
    for i in range(0, point_cloud.shape[0]):
        transformed_point_could[i, :] = transform_point(point_cloud[i, :], transform)

    return transformed_point_could


# -------------------------------------------------------------------------------------------------------------------- #