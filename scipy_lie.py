#!/usr/bin/env python3

import common
import numpy as np

np.set_printoptions(precision=12, suppress=True)

from scipy.spatial.transform import Rotation as R
import scipy.optimize as scopt

import modern_robotics as mr


def get_x(transformation: np.ndarray) -> np.ndarray:
    return mr.se3ToVec(mr.MatrixLog6(transformation))


def get_transformation_matrix(x: np.array) -> np.ndarray:
    return mr.MatrixExp6(mr.VecTose3(x))


def cost_function(x: np.ndarray, initial: np.ndarray, target: np.ndarray) -> float:
    """
    Given a transformation estimate return the cost (the sum of the difference between each point of the point cloud)
    """

    # Convert x to a transformation matrix

    transformation_estimate = get_transformation_matrix(x)
    point_cloud_estimate = common.transform_point_cloud(
        initial, transformation_estimate
    )

    cost = np.zeros(target.shape[0], dtype=float)

    for i in range(0, target.shape[0]):
        cost[i] = np.linalg.norm(target[i, :] - point_cloud_estimate[i, :])

    return cost


def main() -> None:

    initial_point_cloud = common.generate_point_cloud(200)

    # Define expected
    expected_position = np.array([0, 0, 10], dtype=float)
    expected_rotation = R.from_euler("ZYX", np.array([0, 0, 10])).as_matrix()
    expected_transformation = np.eye(4, dtype=float)
    expected_transformation[:3, 3] = expected_position
    expected_transformation[:3, :3] = expected_rotation

    target_point_cloud = common.transform_point_cloud(
        initial_point_cloud, expected_transformation
    )

    # Define initial
    initial_position = np.array([0, 0, 0], dtype=float)
    initial_rotation = R.from_euler("ZYX", np.array([0, 0, 0])).as_matrix()
    initial_transformation = np.eye(4, dtype=float)
    initial_transformation[:3, 3] = initial_position
    initial_transformation[:3, :3] = initial_rotation

    x_init = get_x(initial_transformation)
    print(x_init)

    # Define optimizer
    res = scopt.least_squares(
        fun=cost_function,
        x0=x_init,
        jac="2-point",
        args=(initial_point_cloud, target_point_cloud),
        method="lm",
        ftol=1e-15,
        verbose=1,
    )

    print("Expected:")
    print(expected_transformation)
    print("Actual:")
    print(get_transformation_matrix(res.x))


if __name__ == "__main__":
    main()
